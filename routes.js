const express = require('express');
const router = express.Router();
const book_controller = require('./controller/book');
const user_controller = require("./controller/user")
//import
/* GET home page. */
function middleware(req, res, next) {
    // document.cookie = "username=John Doe";
    // authentication middleware
    // parse login and password from headers
    const b64auth = (req.headers.authorization || '').split(' ')[1] || ''
    const [login, password] = Buffer.from(b64auth, 'base64').toString().split(':')
    // Verify login and password are set and correct
    //todo: here get user based on login, and if everything matches good to go
    user_controller.get_user(login).exec(function (err, user) {
        if (login && password && login === user.email && password === user.password) {
            // Access granted...
            return next()
        }
        // Access denied...
        //res.set('WWW-Authenticate', 'Basic realm="401"')
        //res.status(401).send('Authentication required.') // custom message
        return next()
    })
}
router.get('/', middleware, function (req, res, next) {
    res.json({ express: 'YOUR EXPRESS BACKEND IS CONNECTED TO ANGULAR' });
});

// routing

router.post('/book', middleware, book_controller.book_get_all);                // get all
router.get('/book/:id', middleware, book_controller.book_details);             // get by id
router.post('/book/add', middleware, book_controller.book_create);             // add
router.put('/book/:id', middleware, book_controller.book_update);              // update by id
router.post('/book/delete/:id', middleware, book_controller.book_delete);     // delete by id 

module.exports = router;