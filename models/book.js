const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let BookSchema = new Schema({
    title: { type: String, required: true },
    author: { type: String, required: true },
    isbn: { type: String, required: true },
    date: { type: String, required: true },
    price: { type: Number, required: true, max: 1000000 },
    quantity: { type: Number, required: true, max: 1000000 },
});

module.exports = mongoose.model('Book', BookSchema);