import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BookGuard } from './pages/books/book.guard';

const routes: Routes = [
  {
    path: '', loadChildren: () => import('./pages/landing/landing.module').then(m => m.LandingModule),
  },
  {
    path: 'book', loadChildren: () => import('./pages/books/books.module').then(m => m.BooksModule), canActivate: [BookGuard]
  },
  { path: '**', redirectTo: '/' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
