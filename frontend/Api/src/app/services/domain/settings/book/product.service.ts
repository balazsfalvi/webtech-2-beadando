import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { BookDTO, BookResponseDTO } from './book';
import { BookController } from './book.controller.service';
import { toCreatedBook, toBooks } from './book.model';

@Injectable({
    providedIn: 'root',
})
export class Productervice {

    constructor(private controller: BookController) { }

    getBooks(): Observable<BookDTO[]> {
        return this.controller.getBooks().pipe(map((response: BookDTO[]) => {
            return response ? toBooks(response) : null;
        }));
    }
    /*   title: bookDTO.title,
      author: bookDTO.author,
      isbn: bookDTO.isbn,
      date: bookDTO.date,
      price: bookDTO.price,
      quantity: bookDTO.quantity */
    createBook(title: string, author: string, isbn: string, date: Date, price: number, quantity: number) {
        return this.controller.createBook({ title, author, isbn, date, price, quantity }).pipe(map((response: BookResponseDTO) => {
            return response ? toCreatedBook(response) : null;
        }));
    }

    // tslint:disable-next-line: variable-name
    editBook(_id: string, title: string, author: string, isbn: string, date: Date, price: number, quantity: number) {
        return this.controller.editBook({ _id, title, author, isbn, date, price, quantity }).pipe();
    }

    // tslint:disable-next-line: variable-name
    deleteBook(_id: string) {
        return this.controller.deleteBook(_id).pipe();
    }

}
