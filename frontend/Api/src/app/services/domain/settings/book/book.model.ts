import { BookDTO as BookDTO, BookResponseDTO as BookResponseDTO } from './book';

export interface Book {
    _id: string;
    title: string;
    author: string;
    isbn: string;
    date: Date;
    price: number;
    quantity: number;
}

export interface BookResponse {
    _id: string;
}
export function toBooks(productResponse: BookDTO[]): Book[] {
    return productResponse.map(dto => toBook(dto));
}

export function toBook(bookDTO: BookDTO): Book {
    return {
        _id: bookDTO._id,
        title: bookDTO.title,
        author: bookDTO.author,
        isbn: bookDTO.isbn,
        date: bookDTO.date,
        price: bookDTO.price,
        quantity: bookDTO.quantity
    };
}

export function toCreatedBook(bookDTO: BookResponseDTO): BookResponse {
    return {
        _id: bookDTO._id,
    };
}

