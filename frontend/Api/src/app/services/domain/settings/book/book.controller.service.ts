import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BookResponse } from './book.model';
import { CreateBookDTO as CreateBookDTO, BookDTO as BookDTO, } from './book';

@Injectable()
export abstract class BookController {
    public abstract getBooks(): Observable<BookDTO[]>;
    public abstract createBook(request: CreateBookDTO): Observable<BookResponse>;
    public abstract editBook(request: BookDTO): Observable<BookDTO>;
    // tslint:disable-next-line: variable-name
    public abstract deleteBook(_id: string);
}
