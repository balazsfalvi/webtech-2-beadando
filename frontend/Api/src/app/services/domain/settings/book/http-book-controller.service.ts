import { HttpHeaders, HttpParameterCodec, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { BookController } from './book.controller.service';
import { BookDTO, BookResponseDTO, CreateBookDTO } from './book';
import { BookResponse } from './book.model';

@Injectable()
export class HttpProductController implements BookController {
    private readonly BASE_URL = `http://localhost:5000/api/book`;
    public defaultHeaders = new HttpHeaders();
    public encoder: HttpParameterCodec;
    constructor(private httpClient: HttpClient) { }
    public createBook(request: CreateBookDTO): Observable<BookResponseDTO> {
        return this.httpClient.post(`${this.BASE_URL}/add`, request).pipe(
            map((res: BookResponse) => res)
        );
    }
    public editBook(request: BookDTO): Observable<BookDTO> {
        return this.httpClient.put(`${this.BASE_URL}/${request._id}`, request).pipe(
            map((res: BookDTO) => res)
        );
    }
    // tslint:disable-next-line: variable-name
    public deleteBook(_id: string) {
        return this.httpClient.post(`${this.BASE_URL}/delete/${_id}`, null).pipe();
    }
    public getBooks(): Observable<BookDTO[]> {
        return this.httpClient.post(`${this.BASE_URL}`, null).pipe(
            map((res: BookDTO[]) => res)
        );
    }

}
