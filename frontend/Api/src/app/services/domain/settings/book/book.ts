export interface BookDTO {
  _id: string;
  title: string;
  author: string;
  isbn: string;
  date: Date;
  price: number;
  quantity: number;
}

export interface CreateBookDTO {
  title: string;
  author: string;
  isbn: string;
  date: Date;
  price: number;
  quantity: number;
}

export interface BookResponseDTO {
  _id: string;
}
