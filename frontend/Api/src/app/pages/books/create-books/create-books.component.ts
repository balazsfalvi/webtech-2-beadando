import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { CreateBookDTO } from 'src/app/services/domain/settings/book/book';
import { Productervice } from 'src/app/services/domain/settings/book/product.service';

@Component({
  selector: 'app-create-books',
  templateUrl: './create-books.component.html',
  styleUrls: ['./create-books.component.css']
})
export class CreateBooksComponent implements OnInit {

  constructor(private snackBar: MatSnackBar, private router: Router, private service: Productervice) { }

  bookFormGroup = new FormGroup({
    title: new FormControl('', [Validators.required, Validators.maxLength(20)]),
    author: new FormControl('', [Validators.required, Validators.maxLength(20)]),
    isbn: new FormControl('', [Validators.required]),
    date: new FormControl('', [Validators.required]),
    price: new FormControl('', [Validators.required]),
    quantity: new FormControl('', [Validators.required]),
  });
  book: CreateBookDTO;
  ngOnInit(): void { }

  succes() {
    this.snackBar.open('Sikeres létrehozás', 'Új termék', {
      duration: 1000,
    });
  }
  error() {
    this.snackBar.open('Sikertelen létrehozás', 'A könyv már létezik', {
      duration: 1000,
    });
  }
  createProduct = () => {
    this.book = {
      title: this.bookFormGroup.get('title').value,
      author: this.bookFormGroup.get('author').value,
      isbn: this.bookFormGroup.get('isbn').value,
      date: this.bookFormGroup.get('date').value,
      price: this.bookFormGroup.get('price').value,
      quantity: this.bookFormGroup.get('quantity').value,
    };
    this.service.createBook(this.book.title, this.book.author, this.book.isbn, this.book.date, this.book.price, this.book.quantity).subscribe(val => {
      if (val != null) {
        this.succes();
      }

    }, (err) => {
      this.error();

    });
  }


}
