import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { CurrentUserService } from 'src/app/services/domain/auth/current-user/current-user.service';
import { Book } from 'src/app/services/domain/settings/book/book.model';
import { Productervice } from 'src/app/services/domain/settings/book/product.service';
import { CreateBooksComponent } from '../create-books/create-books.component';

@Component({
  selector: 'app-list-books',
  templateUrl: './list-books.component.html',
  styleUrls: ['./list-books.component.css']
})
export class ListBooksComponent implements OnInit {

  constructor(private router: Router, public dialog: MatDialog, private service: Productervice, private currentUserService: CurrentUserService) { }

  user: string;
  ELEMENT_DATA: Book[];
  displayedColumns: string[];
  dataSource: any;

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngOnInit(): void {
    // console.log(sessionStorage.getItem('currentUser'));
    this.user = this.currentUserService.getEmail();
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  // tslint:disable-next-line: use-lifecycle-interface
  ngAfterViewInit() {
    this.getAllProducts();
  }

  deleteProduct(product: Book): void {

    this.service.deleteBook(product._id).subscribe(val => {
      console.log(val);
      alert('Sikeres törlés!');
      this.getAllProducts();
    });

  }

  editProduct(book: Book): void {
    localStorage.setItem('title', book.title);
    localStorage.setItem('author', book.author);
    localStorage.setItem('isbn', book.isbn);
    localStorage.setItem('price', book.price.toString());
    localStorage.setItem('quantity', book.quantity.toString());
    this.router.navigate(['settings/book', 'edit', book._id]);
  }

  getAllProducts(): void {
    this.service.getBooks().subscribe(val => {
      this.ELEMENT_DATA = val;
      this.displayedColumns = ['title', 'author', 'isbn', 'date', 'price', 'quantity', 'delete'];
      this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
  }
  navigateBack() {
    this.router.navigate(['../../']);
  }
  createProduct(): void {
    const dialogRef = this.dialog.open(CreateBooksComponent, {
      width: '400px',
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.getAllProducts();
    });
  }

  logout(): void {
    sessionStorage.clear();
    this.router.navigate(['../../']);
  }

}
