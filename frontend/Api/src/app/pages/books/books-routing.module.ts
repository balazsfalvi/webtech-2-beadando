import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateBooksComponent } from './create-books/create-books.component';
import { ListBooksComponent } from './list-books/list-books.component';

const routes: Routes = [
    { path: 'books', component: ListBooksComponent },

];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class BooksRoutingModule { }
